package modelo;

public class Account
{
    private int id_account;
    private int id_customer;
    private String full_nameString;
    private double total_amount;
    
    public int getId_account() {
        return this.id_account;
    }
    
    public void setId_account(final int id_account) {
        this.id_account = id_account;
    }
    
    public int getId_customer() {
        return this.id_customer;
    }
    
    public void setId_customer(final int id_customer) {
        this.id_customer = id_customer;
    }
    
    public String getFull_nameString() {
        return this.full_nameString;
    }
    
    public void setFull_nameString(final String full_nameString) {
        this.full_nameString = full_nameString;
    }
    
    public double getTotal_amount() {
        return this.total_amount;
    }
    
    public void setTotal_amount(final double total_amount) {
        this.total_amount = total_amount;
    }
}