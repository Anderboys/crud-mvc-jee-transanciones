package modelo;

public class TransactionDeposit
{
    private Integer id;
    private Integer accountId;
    private double amount;
    private String type;
    
    public Integer getId() {
        return this.id;
    }
    
    public void setId(final Integer id) {
        this.id = id;
    }
    
    public Integer getAccountId() {
        return this.accountId;
    }
    
    public void setAccountId(final Integer accountId) {
        this.accountId = accountId;
    }
    
    public double getAmount() {
        return this.amount;
    }
    
    public void setAmount(final double amount) {
        this.amount = amount;
    }
    
    public String getType() {
        return this.type;
    }
    
    public void setType(final String type) {
        this.type = type;
    }
}