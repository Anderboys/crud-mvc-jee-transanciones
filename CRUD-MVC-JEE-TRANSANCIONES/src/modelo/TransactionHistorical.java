package modelo;

public class TransactionHistorical
{
    private String _id;
    private Double amount;
    private String type;
    private int accountId;
    private String _class;
    
    public String get_id() {
        return this._id;
    }
    
    public void set_id(final String _id) {
        this._id = _id;
    }
    
    public Double getAmount() {
        return this.amount;
    }
    
    public void setAmount(final Double amount) {
        this.amount = amount;
    }
    
    public String getType() {
        return this.type;
    }
    
    public void setType(final String type) {
        this.type = type;
    }
    
    public int getAccountId() {
        return this.accountId;
    }
    
    public void setAccountId(final int accountId) {
        this.accountId = accountId;
    }
    
    public String get_class() {
        return this._class;
    }
    
    public void set_class(final String _class) {
        this._class = _class;
    }
}