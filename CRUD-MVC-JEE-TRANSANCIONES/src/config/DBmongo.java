package config;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoDatabase;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DB;

public class DBmongo
{
    DB DataBase;
    DBCollection collection;
    BasicDBObject document;
    
    public DBmongo() {
        this.document = new BasicDBObject();
    }
    
    public MongoDatabase CNXMongoAtlas() {
        MongoDatabase database = null;
        final MongoClientURI uri = new MongoClientURI("mongodb+srv://abel:abc123.@abelanders.1wpvt.mongodb.net/db_account?retryWrites=true&w=majority");
        final MongoClient mongoClient = new MongoClient(uri);
        database = mongoClient.getDatabase("db_account");
        return database;
    }
}