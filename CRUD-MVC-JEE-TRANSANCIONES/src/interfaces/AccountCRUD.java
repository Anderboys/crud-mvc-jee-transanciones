package interfaces;

import modelo.Account;
import java.util.List;

public interface AccountCRUD
{
    List listar();
    
    List listarByDNI(final String p0);
    
    Account BuscarByID(final String p0);
    
    void agregar(final Account p0);
    
    void edit(final Account p0);
    
    Boolean eliminar(final String p0);
}