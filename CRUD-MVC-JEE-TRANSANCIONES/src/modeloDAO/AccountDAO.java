package modeloDAO;

import modelo.Account;
import java.util.ArrayList;
import java.util.List;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.Connection;
import config.DBsqlserver;
import interfaces.AccountCRUD;

public class AccountDAO implements AccountCRUD
{
    DBsqlserver cn;
    Connection con;
    PreparedStatement ps;
    ResultSet rs;
    int resp;
    
    public AccountDAO() {
        this.cn = new DBsqlserver();
    }
    
    public List listar() {
        System.out.println("From AccountDAO Method -> List listar() ");
        final String sql2 = "SELECT A.id_account,A.total_amount ,C.full_name,A.id_customer  FROM account A INNER JOIN customer C ON A.id_customer = C.id_customer";
        final List<Account> lista = new ArrayList<Account>();
        try {
            this.con = this.cn.ConexionSqlServer();
            if (this.con != null) {
                System.out.println("Conexi\u00f3n Exitosa");
            }
            else {
                System.out.println("Fall\u00f3 Conexi\u00f3n");
            }
            this.ps = this.con.prepareStatement(sql2);
            this.rs = this.ps.executeQuery();
            while (this.rs.next()) {
                System.out.println("valor id_account = " + this.rs.getInt(1));
                System.out.println("valor total_amount = " + this.rs.getDouble(2));
                System.out.println("valor full_name = " + this.rs.getString(3));
                System.out.println("valor id_customer = " + this.rs.getInt(4));
                final Account acc = new Account();
                acc.setId_account(this.rs.getInt(1));
                acc.setTotal_amount(this.rs.getDouble(2));
                acc.setFull_nameString(this.rs.getString(3));
                acc.setId_customer(this.rs.getInt(4));
                lista.add(acc);
            }
        }
        catch (Exception ex) {}
        return lista;
    }
    
    public List listarByDNI(final String dni) {
        return null;
    }
    
    public Account BuscarByID(final String identifier) {
        return null;
    }
    
    public void agregar(final Account account) {
    }
    
    public void edit(final Account account) {
    }
    
    public Boolean eliminar(final String id) {
        return null;
    }
}