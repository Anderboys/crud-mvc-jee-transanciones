package modeloDAO;

import com.mongodb.client.model.Filters;
import org.bson.conversions.Bson;
import com.mongodb.client.MongoCursor;
import org.bson.Document;
import java.util.ArrayList;
import java.util.List;
import modelo.TransactionHistorical;
import com.mongodb.client.MongoDatabase;
import config.DBmongo;
import interfaces.TransactionCRUD;

public class TransactionDAO implements TransactionCRUD {
	DBmongo dbConfig;
	MongoDatabase DBO;
	TransactionHistorical trans;

	public TransactionDAO() {
		this.dbConfig = new DBmongo();
		this.DBO = null;
		this.trans = new TransactionHistorical();
	}

	public List listarTransac() {
		System.out.println(" TransactionDAO -> metodo -> listar() ");
		final List<TransactionHistorical> listaTransaction = new ArrayList<TransactionHistorical>();
		this.DBO = this.dbConfig.CNXMongoAtlas();
		if (this.DBO != null) {
			System.out.println("Conexion Ok");
		} else {
			System.out.println("Error Conexion");
		}
		Document document = null;
		final MongoCursor<Document> result = (MongoCursor<Document>) this.DBO.getCollection("transaction").find()
				.iterator();
		System.out.println(result);
		while (result.hasNext()) {
			document = (Document) result.next();
			System.out.println(document.toJson().toString());
			final TransactionHistorical transac = new TransactionHistorical();
			transac.set_id(document.getString((Object) "_id"));
			transac.setAmount(document.getDouble((Object) "amount"));
			transac.setType(document.getString((Object) "type"));
			transac.setAccountId((int) document.getInteger((Object) "accountId"));
			transac.set_class(document.getString((Object) "_class"));
			listaTransaction.add(transac);
		}
		return listaTransaction;
	}

	public List listByAccountId(final Integer accountId) {
		System.out.println(" EmpleadoDAO -> public List listByAccountId (Integer " + accountId + ")");
		final ArrayList<TransactionHistorical> listaTransaction = new ArrayList<TransactionHistorical>();
		this.DBO = this.dbConfig.CNXMongoAtlas();
		if (this.DBO != null) {
			System.out.println("Conexion Ok");
		} else {
			System.out.println("Error Conexion");
		}
		Document document = null;
		Bson filter = null;
		final Bson Fdni = (Bson) new Document("accountId", (Object) accountId);
		filter = Filters.and(new Bson[] { Fdni });
		final MongoCursor<Document> result = (MongoCursor<Document>) this.DBO.getCollection("transaction").find(filter)
				.iterator();
		while (result.hasNext()) {
			document = (Document) result.next();
			System.out.println(document.toJson().toString());
			final TransactionHistorical transac = new TransactionHistorical();
			transac.set_id(document.getString((Object) "_id"));
			transac.setAmount(document.getDouble((Object) "amount"));
			transac.setType(document.getString((Object) "type"));
			transac.setAccountId((int) document.getInteger((Object) "accountId"));
			transac.set_class(document.getString((Object) "_class"));
			listaTransaction.add(transac);
		}
		return listaTransaction;
	}
}