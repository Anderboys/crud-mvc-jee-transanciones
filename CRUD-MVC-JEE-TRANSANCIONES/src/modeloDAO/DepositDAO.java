// 
// Decompiled by Procyon v0.5.36
// 

package modeloDAO;

import modelo.TransactionDeposit;
import java.util.ArrayList;
import java.util.List;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.Connection;
import config.DBpostgress;
import interfaces.DepositCRUD;

public class DepositDAO implements DepositCRUD
{
    DBpostgress cn;
    Connection con;
    PreparedStatement ps;
    ResultSet rs;
    int resp;
    
    public DepositDAO() {
        this.cn = new DBpostgress();
    }
    
    public List listarDeposit() {
        System.out.println("From DepositDAO Method -> List listar() ");
        final String sql2 = "SELECT id, account_id, amount, type FROM transaction;";
        final List<TransactionDeposit> lista = new ArrayList<TransactionDeposit>();
        try {
            this.con = this.cn.ConexionPostgress();
            if (this.con != null) {
                System.out.println("Conexi\u00f3n Exitosa");
            }
            else {
                System.out.println("Fall\u00f3 Conexi\u00f3n");
            }
            this.ps = this.con.prepareStatement(sql2);
            this.rs = this.ps.executeQuery();
            while (this.rs.next()) {
                System.out.println("valor id = " + this.rs.getInt(1));
                System.out.println("valor account_id = " + this.rs.getInt(2));
                System.out.println("valor amount = " + this.rs.getDouble(3));
                System.out.println("valor type = " + this.rs.getString(4));
                final TransactionDeposit deposit = new TransactionDeposit();
                deposit.setId(Integer.valueOf(this.rs.getInt(1)));
                deposit.setAccountId(Integer.valueOf(this.rs.getInt(2)));
                deposit.setAmount(this.rs.getDouble(3));
                deposit.setType(this.rs.getString(4));
                lista.add(deposit);
            }
        }
        catch (Exception ex) {}
        return lista;
    }
}