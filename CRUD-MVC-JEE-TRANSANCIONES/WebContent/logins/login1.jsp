<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Login</title>

<link rel="canonical"
	href="https://getbootstrap.com/docs/4.6/examples/sign-in/">

<!-- Bootstrap core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/signin.css" rel="stylesheet">


</head>

<body class="text-center">

	<form class="form-signin">
		<img src="img/thunder.png" alt="90" width="280" />
		<h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>
		<label for="inputEmail" class="sr-only">Email address</label>
		 <input type="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus>
	    <label	for="inputPassword" class="sr-only">Password</label>
	     <input	type="password" id="inputPassword" class="form-control"	placeholder="Password" required>
		<div class="checkbox mb-3">
			<label> <input type="checkbox" value="remember-me">
				Remember me
			</label>
		</div>
		<button class="btn btn-lg btn-primary btn-block" type="submit">Sign
			in</button>
		<p class="mt-5 mb-3 text-muted">&copy; 2017-2022</p>
	</form>


</body>
</html>