<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<!-- THIS IS FOR Bootstrap core CSS AND JS -->

<link href="twitter-bootstrap/css/bootstrap.css" rel="stylesheet"
	type="text/css" />
<script src="twitter-bootstrap/js/bootstrap.js" type="text/javascript"></script>

<title>Depositos</title>
</head>
<body>
	<h3 class="mb-0">Transacciones</h3>
	<h4>Estas realizando un Deposit From Docker - PostgreSQL</h4>
		<br></br>

		<form>
			<div class="form-group row">
				<label  class="col-sm-2 col-form-label">Tipo de Operación</label>
				<div class="col-sm-4">
					<input type="text" class="form-control" placeholder="Deposito O Retiro">
				</div>
			</div>
			<div class="form-group row">
				<label  class="col-sm-2 col-form-label">Monto</label>
				<div class="col-sm-4">
					<input type="text" class="form-control" placeholder="Ingresar Monto">
				</div>
			</div>
			
			<div class="form-group row">
			
				<div class="col-sm-4">				
				<a class="btn btn-info"	href="Controlador?menu=Account&accion=Listar">Volver</a>
				</div>
				
				<div class="col-sm-4">
			<button type="button" class="btn btn-success">Registrar</button>
				</div>
				
			</div>
			
		</form>
</body>
</html>