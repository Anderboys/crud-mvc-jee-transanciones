
<!--   Para trabajar con   ->  c:forEach  <-   -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%-- <%@page import="modeloDAO.EmpleadoDAO"%> --%>
<%-- <%@ page import="modelo.Empleado"%> --%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.Iterator"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Depositos</title>

<%--   THIS IS FOR DatePicker--%>

<!--    THIS IS JQUERY   -->
<link rel="stylesheet"
	href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>



<!-- THIS IS FOR Bootstrap core CSS AND JS -->

<!--         <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"> -->
<!--         <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script> -->
<link href="twitter-bootstrap/css/bootstrap.css" rel="stylesheet"
	type="text/css" />
<script src="twitter-bootstrap/js/bootstrap.js" type="text/javascript"></script>


<!-- THIS IS FOR FOOTABLE  CSS AND JS -->
<link href="css/plugins/footable/footable.core.css" rel="stylesheet"
	type="text/css" />
<!-- 	              THIS  footable.all.min.js ejecuta la Busqueda Total  id="filter" Buscar -->
<script src="js/plugins/footable/footable.all.min.js"
	type="text/javascript"></script>

</head>
<body>

	<h3 class="mb-0">Listado Depositos</h3>
	<h4>From Docker - PostgreSQL</h4>
	<!--  FORMULARIO LISTAR Y EDITAR col-sm-9  -->

	<div class="col-sm-9">
		<!-- 			<div class="card-body"> -->
		<div class="table-responsive">
			<table id='tableDepositList'
				class="footable table table-striped table-hover" data-page-size="20"
				data-filter="#filter">
				<!-- 		<table class="table table-hover"> -->

				<thead>
					<tr>
						<th>ID</th>
						<th>ID Cuenta</th>
						<th>Total</th>
						<th>Tipo</th>

					</tr>
				</thead>

				<tbody>
					<c:forEach var="dep" items="${depositList}">
						<tr>

							<td>${dep.getId()}</td>
							<td>${dep.getAccountId()}</td>
							<td>${dep.getAmount()}</td>
							<td>${dep.getType()}</td>

						</tr>
					</c:forEach>


				</tbody>

				<!-- 					   CODIGO DE PAGINACION  -->
				<tfoot>
					<tr>
						<td colspan="10">
							<ul class="pagination pull-right"></ul>
						</td>
					</tr>
				</tfoot>
				<!-- 		            -----------------------  -->

			</table>
		</div>
	</div>




	<!-- FUNCION DE FOOTABLE -->
	<script lang="javascript">
		$(function() {
			$('.footable').footable();
		});
	</script>
	<!--------------------->



	<!--------    LLAMA OTRO FORMULARIO  listaempleado.jsp   ------->
	<!--  		 <iframe name="myFrame2" width="100%" height="500px" frameborder="0"></iframe> -->


</body>
</html>